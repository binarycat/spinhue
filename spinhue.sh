#!/bin/sh

cd $(dirname $(readlink -f $0))

out="$1"
if [ -z "$out" ] || [ - = "$out" ]; then
	out=png:-
fi

seed="hex $2 decimal"
if [ -z "$2" ]; then
	seed=rand
fi

size="$3"
if [ -z "$3" ]; then
	size="200 200"
fi

gforth -e "include rgb24.fs $seed $size ppm-rainbow bye" |
	convert /dev/stdin "$out"

