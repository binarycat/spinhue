# spinhue wallpaper generator

this is a simple forth program that generates colorful images.

## dependancies

* gforth (tested with 0.7.3)
* ImageMagik (for converting ppm to png)

## running/installation

the recommended way of installing is by creating a symlink to spinhue.sh somewhere in $PATH.
this is because it depends on certain files (such as rgb24.fs) being in the same dir as the executable.

spinhue takes the following  arguments:
* OUPUT: filename to write the output to, - or empty for stdout
* SEED: seed to use for wallpaper generation.

the seed used is stored in the png comment field.

